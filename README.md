# Tipp10 (Qt6) - unofficial

This is a fork of the official Version V2.1.0 from https://www.tipp10.com

Patches are welcome.

## Installation

### Flathub
See https://flathub.org/apps/details/com.gitlab.tipp10.tipp10

### Arch Linux
```sh
sudo pacman -S tipp10
```

### From Source
```sh
git clone https://gitlab.com/tipp10/tipp10.git
cd tipp10
mkdir build
cd build
cmake ..
make
```
