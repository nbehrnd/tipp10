/*
Copyright (c) 2006-2009, Tom Thielicke IT Solutions
Copyright (c) 2021, André Apitzsch

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "regexpdialog.h"
#include "ui_regexpdialog.h"

#include <QPushButton>
#include <QSettings>

#include "sql/trainingsql.h"

RegExpDialog::RegExpDialog(QString layout, QWidget* parent)
    : QDialog(parent)
    , ui(new Ui::RegExpDialog)
{
    ui->setupUi(this);

    currentLayout = layout;

    readSettings();

    QPushButton* resetButton
        = ui->buttonBox->button(QDialogButtonBox::RestoreDefaults);
    connect(resetButton, SIGNAL(clicked()), this, SLOT(getDefault()));

    connect(
        ui->buttonBox, &QDialogButtonBox::accepted, this, &RegExpDialog::save);
}

RegExpDialog::~RegExpDialog() { delete ui; }

void RegExpDialog::save()
{
    writeSettings();
    this->accept();
}

void RegExpDialog::getDefault()
{
    TrainingSql* trainingSql = new TrainingSql();
    ui->lineRegExp->setText(
        trainingSql->getKeyboardLayoutRegexpRoutine(currentLayout));
    ui->lineReplace->setText(
        trainingSql->getKeyboardLayoutReplaceRoutine(currentLayout));
}

void RegExpDialog::readSettings()
{
#if APP_PORTABLE
    QSettings settings(
        QCoreApplication::applicationDirPath() + "/portable/settings.ini",
        QSettings::IniFormat);
#else
    QSettings settings;
#endif
    settings.beginGroup("main");
    QString regexp = settings.value("layout_regexp", "NULL").toString();
    QString replace = settings.value("layout_replace", "NULL").toString();
    settings.endGroup();
    TrainingSql* trainingSql = new TrainingSql();

    if (regexp == "NULL") {
        regexp = trainingSql->getKeyboardLayoutRegexpRoutine(currentLayout);
    }
    if (replace == "NULL") {
        replace = trainingSql->getKeyboardLayoutReplaceRoutine(currentLayout);
    }
    ui->lineRegExp->setText(regexp);
    ui->lineReplace->setText(replace);
}

void RegExpDialog::writeSettings()
{
#if APP_PORTABLE
    QSettings settings(
        QCoreApplication::applicationDirPath() + "/portable/settings.ini",
        QSettings::IniFormat);
#else
    QSettings settings;
#endif
    settings.beginGroup("main");
    settings.setValue("layout_replace", ui->lineReplace->text());
    settings.setValue("layout_regexp", ui->lineRegExp->text());
    settings.endGroup();
}
