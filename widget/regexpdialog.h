/*
Copyright (c) 2006-2009, Tom Thielicke IT Solutions
Copyright (c) 2021, André Apitzsch

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef REGEXPDIALOG_H
#define REGEXPDIALOG_H

#include <QDialog>

namespace Ui {
class RegExpDialog;
}

class RegExpDialog : public QDialog {
    Q_OBJECT

public:
    explicit RegExpDialog(QString layout, QWidget* parent = nullptr);
    ~RegExpDialog();

private slots:
    void getDefault();

private:
    void save();
    void readSettings();
    void writeSettings();

    Ui::RegExpDialog* ui;
    QString currentLayout;
};

#endif // REGEXPDIALOG_H
