/*
Copyright (c) 2006-2011, Tom Thielicke IT Solutions

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.
*/

/****************************************************************
**
** File name: defines.h
**
****************************************************************/

#ifndef DEFINES_H
#define DEFINES_H

#if APP_PORTABLE || defined(APP_WIN)
    #define TIPP10_ROOTDIR QApplication::applicationDirPath()
#else

    #ifndef INSTALLPREFIX
        #define INSTALLPREFIX "/usr/local"
    #endif
    // must end with a trailing /
    #define TIPP10_ROOTDIR QString(INSTALLPREFIX "/share/tipp10/")
#endif


// Languages
#define APP_STD_LANGUAGE_LAYOUT		"de_qwertz_win"
#define APP_STD_LANGUAGE_LESSON		"de_de_qwertz"

// Common program constants
#define APP_NAME_INTERN				"TIPP10"
#define APP_NAME 					"TIPP10"
#define APP_URL 					"https://gitlab.com/tipp10/tipp10"
#define APP_DB 						"tipp10v2.template"
#define APP_USER_DB					"tipp10v2.db"
#define APP_VERSION					"3.3.0"

// Update constants
#define UPDATE_URL_SQL 				"/update/sql.tipp10v210.utf"
#define COMPILED_UPDATE_VERSION		33
#define COMPILED_UPDATE_FILENAME	"tipp10v2.template"

// Lesson text constants
#define NUM_TOKEN_UNTIL_REFRESH 	25
#define NUM_TOKEN_UNTIL_NEW_LINE 	35
#define NUM_INTELLIGENT_QUERYS 		2
#define TOKEN_NEW_LINE 				0x00b6
#define TOKEN_TAB					0x2192
#define TOKEN_BACKSPACE				0x00b9 //0x00ac

// Constants for dynamic training
#define NUM_TEXT_UNTIL_REPEAT 		10
#define BORDER_LESSON_IS_SENTENCE 	7
#define LAST_LESSON				 	18 // lesson with training of all characters
#define NUMPAD_LESSON_START			19
#define SYNCHRON_DB_WHILE_TRAINING 	false

// Standard constants
#define LESSON_TIMELEN_STANDARD 	5
#define LESSON_TOKENLEN_STANDARD	500
#define TICKERSPEED_STANDARD 		2
#define TICKER_COLOR_FONT    		"#000000"
#define TICKER_COLOR_BG     		"#FFFFFF"
#define TICKER_COLOR_CURSOR    		"#CDCDCD"
#define METRONOM_STANDARD           60

// Font format
#define FONT_STANDARD               "Arial"
#define FONT_SIZE_TICKER            18
#define FONT_SIZE_TICKER_PAUSE      16
#define FONT_SIZE_STATUS            8
#define FONT_SIZE_PROGRESS          8
#define FONT_SIZE_PROGRESS_LESSON   6
#define FONT_SIZE_FINGER            10

// Window dimensions and icon filename
#define APP_WIDTH_STANDARD      750
#define APP_HEIGHT_STANDARD     520
#define APP_WIDTH_SMALL         680
#define APP_HEIGHT_SMALL        210

#endif // DEFINES_H
